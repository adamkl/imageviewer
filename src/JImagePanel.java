import java.awt.Graphics;
import java.awt.LayoutManager;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class JImagePanel extends JPanel {
	private BufferedImage img;

public JImagePanel() {
		super();
		// TODO Auto-generated constructor stub
		setSize(400, 300);
	}
	public JImagePanel(boolean isDoubleBuffered) {
		super(isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}
	public JImagePanel(LayoutManager layout, boolean isDoubleBuffered) {
		super(layout, isDoubleBuffered);
		// TODO Auto-generated constructor stub
	}
	public JImagePanel(LayoutManager layout) {
		super(layout);
		// TODO Auto-generated constructor stub
	}
protected void paintComponent(Graphics g) {
	// TODO Auto-generated method stub
	super.paintComponent(g);
    
	g.drawImage(img, 0, 0, null);
}
  public void setImage(){
	  try{
	  img=ImageIO.read(new File("panorama.png"));
//	  int h=img.getHeight();
//	  int w=img.getWidth();
//	  setSize(400, 300);
	  repaint();
	  }
	  catch(IOException e){
		  JOptionPane.showMessageDialog(this, e.getMessage());
	  }
  }
}
